#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
from datetime import datetime
import web_api_conf as conf
import hashlib


class AssetDatabase:
    def __init__(self):
        self.database_path = conf.kAssetDatabase

    def update_auto(self):
        if not os.path.exists(self.database_path):
            print("Error: no asset DB file exists on update_auto: {}.".format(self.database_path))
            return False

        if os.path.exists(conf.kLockDir):
            print("Error: cannot update asset DB on update auto because writing it by others.")
            return False
        else:
            os.mkdir(conf.kLockDir)

        f = open(self.database_path, "r")
        jsonString = f.read()
        if jsonString == "":
            print("asset_db.json not exists.")
            jsonDict = {}
        else:
            jsonDict = json.loads(jsonString)
        f.close()

        for asset_id, data in jsonDict.items():
            asset_path = data['filepath']
            if not os.path.exists(asset_path):
                print("Asset {} file not exists: {}".format(asset_id, asset_path))
                print("Asset {} is removed from DB.".format(asset_id))
                data.pop(asset_id, None)

        jsonString = json.dumps(jsonDict, indent=2)

        f = open(self.database_path, "w")
        f.write(jsonString)
        f.close()

        os.rmdir(conf.kLockDir)
        print("Update finished.")
        return True

    def make_asset_id(self, asset_path, timestamp=None):
        if timestamp is None:
            timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
        hash_object = hashlib.md5(asset_path.encode())
        asset_id = hash_object.hexdigest()[0:8] + "id" + timestamp
        return asset_id

    def get_format(self, asset_path):
        # get asset format
        filename, ext = os.path.splitext(asset_path)
        return ext[1:]

    def write(self, asset_id, asset_path, model_name, params):
        if not os.path.exists(self.database_path):
            print("Warning: no asset DB file exists on write: {}.".format(self.database_path))
            with open(self.database_path, "w") as f:
                f.write("")

        if os.path.exists(conf.kLockDir):
            print("Error: cannot update asset DB on write because writing it by others.")
            return False
        else:
            os.mkdir(conf.kLockDir)

        f = open(self.database_path, "r")
        jsonString = f.read()
        if jsonString == "":
            jsonDict = {}
        else:
            jsonDict = json.loads(jsonString)
        f.close()

        format = self.get_format(asset_path)
        updatetime = datetime.now().strftime("%Y%m%d%H%M%S")

        # set model_name in params for search
        params['model_name'] = model_name
        jsonDict[asset_id] = {'filepath': asset_path, 'id':asset_id, 'format':format, 'model_name':model_name, 'params':params, 'update_time':updatetime}
        jsonString = json.dumps(jsonDict, indent=2)

        f = open(self.database_path, "w")
        f.write(jsonString)
        f.close()

        os.rmdir(conf.kLockDir)
        return True

    def read(self, asset_id):
        if not os.path.exists(self.database_path):
            print("Error: no asset DB file exists on read: {}.".format(self.database_path))
            return {}

        if os.path.exists(conf.kLockDir):
            print("Error: cannot update asset DB on read because writing it by others.")
            return {}
        else:
            os.mkdir(conf.kLockDir)

        f = open(self.database_path, "r")
        jsonString = f.read()
        if jsonString == "":
            print("Error: Asset DB is empty.")
            f.close()
            os.rmdir(conf.kLockDir)
            return
        jsonDict = json.loads(jsonString)
        # readtime = datetime.now().strftime("id%Y%m%d%H%M%S")
        asset_data = jsonDict.get(asset_id)

        f.close()
        os.rmdir(conf.kLockDir)
        return asset_data

    def readAll(self):
        if not os.path.exists(self.database_path):
            print("Error: no asset DB file exists on read: {}.".format(self.database_path))
            return {}

        if os.path.exists(conf.kLockDir):
            print("Error: cannot update asset DB on readAll because writing it by others.")
            return {}
        else:
            os.mkdir(conf.kLockDir)


        f = open(self.database_path, "r")
        jsonString = f.read()
        if jsonString == "":
            print("Error: Asset DB is empty.")
            f.close()
            os.rmdir(conf.kLockDir)
            return

        jsonDict = json.loads(jsonString)

        f.close()
        os.rmdir(conf.kLockDir)
        return jsonDict

    def searchAsset(self, query, asset_data):
        print("searchAsset query: {}".format(query.items()))
        for qkey, qval in query.items():
            print("search")
            print(qkey, qval)
            if qval is None:
                print("qval is None")
                continue
            if asset_data.get(qkey) is None:
                print("asset_data.get(qkey) is None")
                return False
            else:
                print("qval", qval)
                print("asset_data.get(qkey)", asset_data.get(qkey))
                if qval != asset_data.get(qkey):
                    print("not same")
                    return False
        return True

    def search(self, query):
        print("Search query: {}".format(query))
        jsonDict = self.readAll()
        resultDict = {}
        for asset_id, asset_data in jsonDict.items():
            print("check asset: ")
            print(asset_data)
            params = asset_data['params']
            #isContain = True
            #for qkey, qval in query.items():
            isContainInParam = self.searchAsset(query, params)
            print("is contain  isContainInParam")
            print( isContainInParam)
            if isContainInParam:
                resultDict[asset_id] = asset_data
                print(asset_data)
        return resultDict
