#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, time
import nn_models
import performance_rnn
import web_api_conf as conf
import assetdb

def generate_performances(model_name, model_path, dest_dir):

    def generate_performance(model_name, model_path, dest_dir, duration, temperature):
        json_dir_tmpl = dest_dir + "json/d{}_t{}/"
        midi_dir_tmpl = dest_dir + "mid/d{}_t{}/"
        json_dir = json_dir_tmpl.format(duration, temperature)
        midi_dir = midi_dir_tmpl.format(duration, temperature)
        print("performance_rnn: createTrack duration:{}[sec] temperature:{}".format(duration, temperature))
        json_filepath, midi_filepath, timestamp = performance_rnn.createTrack(model_name, model_path, None, duration, temperature, json_dir, midi_dir)
        print("generated: {} {}".format(json_filepath, midi_filepath))

        db = assetdb.AssetDatabase()
        params = {'duration':duration, 'temperature':temperature}
        asset_id = db.make_asset_id(json_filepath, timestamp)
        db.write(asset_id, json_filepath, model_name, params=params)
        asset_id = db.make_asset_id(midi_filepath, timestamp)
        db.write(asset_id, midi_filepath, model_name, params=params)

    if not os.path.exists(model_path):
        print("Error: Model file not found: {}".format(model_path))
        return
    print("Load model: {}".format(model_path))

    duration = 30.0
    temperature = 0.7
    generate_performance(model_name, model_path, dest_dir, duration, temperature)
    temperature = 1.0
    generate_performance(model_name, model_path, dest_dir, duration, temperature)
    temperature = 2.0
    generate_performance(model_name, model_path, dest_dir, duration, temperature)

    duration = 60.0
    temperature = 0.7
    generate_performance(model_name, model_path, dest_dir, duration, temperature)
    temperature = 1.0
    generate_performance(model_name, model_path, dest_dir, duration, temperature)
    temperature = 2.0
    generate_performance(model_name, model_path, dest_dir, duration, temperature)



def execute():
    # download model files
    model_dir = conf.kModelDir
    nn_models.download_models(model_dir)

    db = assetdb.AssetDatabase()
    db.update_auto()

    # start generation
    cnt = 0
    while(True):
        generate_performances(conf.kPerformanceRNNModelName, conf.kPerformanceRNNModelPath, conf.kPerformanceRNNWorkDir)

        sec = 10
        time.sleep(sec)
        if cnt > 1000:
            break

execute()
