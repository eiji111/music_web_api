#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, glob, shutil

from magenta.music import midi_io
from google.protobuf.json_format import MessageToJson
import json
from datetime import datetime
import assetdb
from ml_midi import*
import numpy as np

def createAsset(model_name, model_path, numsteps, json_dir, midi_dir):
    if not os.path.exists(json_dir):
        try:
            os.makedirs(json_dir)
        except OSError:
            print("failed to make dir {}.".format(json_dir))
            return
    if not os.path.exists(midi_dir):
        try:
            os.makedirs(midi_dir)
        except OSError:
            print("failed to make dir {}.".format(midi_dir))
            return

    # make temp dir
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")

    tmp_dir = midi_dir + 'tmp{}/'.format(timestamp)
    if not os.path.exists(tmp_dir):
        try:
            os.makedirs(tmp_dir)
        except OSError:
            print("failed to make dir {}.".format(tmp_dir))
            return

    print("create drum asset")

    os.system("drums_rnn_generate --config='drum_kit' --bundle_file=mag/drum_kit_rnn.mag --output_dir={} --num_outputs=1 --num_steps={} --primer_drums='[(36,)]'".format(tmp_dir,numsteps))

    # get tmp midi file
    midifiles = glob.glob(tmp_dir + "*.mid")
    if len(midifiles) == 0:
        print("Error: failed to create drum score.")
        shutil.rmtree(tmp_dir)
        return None, None, timestamp

    # output midifile

    midi_filepath = midi_dir + "drum{}.mid".format(timestamp)
    shutil.move(midifiles[0], midi_filepath)
    shutil.rmtree(tmp_dir)

    seq = NoteSequence()
    seq.load_from_file(midi_filepath)
    jsonDict = seq.to_dict()

    json_filepath = json_dir + "drum{}.json".format(timestamp)
    # attach asset id
    asset_id = assetdb.AssetDatabase().make_asset_id(json_filepath, timestamp)
    jsonDict['asset_id'] = asset_id

    def default(o):
        if isinstance(o, np.int64): return int(o)
        raise TypeError

    jsonString = json.dumps(jsonDict, default=default)
    with open(json_filepath, "w") as f:
        f.write(jsonString)

    return json_filepath, midi_filepath, timestamp
