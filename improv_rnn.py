#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, glob, shutil
import magenta
from magenta.models.improv_rnn import improv_rnn_config_flags
from magenta.models.improv_rnn import improv_rnn_model
from magenta.models.improv_rnn import improv_rnn_sequence_generator
from magenta.protobuf import generator_pb2
from magenta.protobuf import music_pb2

from magenta.music import midi_io
from google.protobuf.json_format import MessageToJson
import json
from datetime import datetime
import assetdb
from ml_midi import*
import numpy as np

def createAsset(model_name, model_path, primer_melody, backing_chords, json_dir, midi_dir):
    if not os.path.exists(json_dir):
        try:
            os.makedirs(json_dir)
        except OSError:
            print("failed to make dir {}.".format(json_dir))
            return
    if not os.path.exists(midi_dir):
        try:
            os.makedirs(midi_dir)
        except OSError:
            print("failed to make dir {}.".format(midi_dir))
            return

    print("improv_rnn_generate: primer_melody={}, backing_chords={}".format(primer_melody, backing_chords))
    tmp_dir = midi_dir + 'tmp/'
    os.system("improv_rnn_generate --config='chord_pitches_improv' --bundle_file={} --output_dir={} --num_outputs=1 --primer_melody={} --backing_chords={} --render_chords".format(model_path, tmp_dir, primer_melody, backing_chords))

    # get tmp midi file
    midifiles = glob.glob(tmp_dir + "*.mid")
    # output midifile
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    midi_filepath = midi_dir + "improv{}.mid".format(timestamp)
    shutil.move(midifiles[0], midi_filepath)

    seq = NoteSequence()
    seq.load_from_file(midi_filepath)
    jsonDict = seq.to_dict()
    print(jsonDict)
    json_filepath = json_dir + "improv{}.json".format(timestamp)
    # attach asset id
    asset_id = assetdb.AssetDatabase().make_asset_id(json_filepath, timestamp)
    jsonDict['asset_id'] = asset_id

    def default(o):
        if isinstance(o, np.int64): return int(o)
        raise TypeError
    jsonString = json.dumps(jsonDict, default=default)
    with open(json_filepath, "w") as f:
        f.write(jsonString)

    return json_filepath, midi_filepath, timestamp
    '''
    # output track as json file
    #
    jsonString = MessageToJson(sequence)
    jsonDict = json.loads(jsonString)
    json_filepath = json_dir + "performance{}.json".format(timestamp)
    # attach asset id
    #asset_id = assetdb.AssetDatabase().make_asset_id(json_filepath, timestamp)
    #jsonDict['asset_id'] = asset_id

    # output track as json file
    jsonString = MessageToJson(sequence)
    jsonDict = json.loads(jsonString)
    with open("static/json/improv.json", "w") as f:
        f.write(jsonString)
    '''
