#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, glob, shutil
import magenta
from magenta.models.improv_rnn import improv_rnn_config_flags
from magenta.models.improv_rnn import improv_rnn_model
from magenta.models.improv_rnn import improv_rnn_sequence_generator
from magenta.protobuf import generator_pb2
from magenta.protobuf import music_pb2

from magenta.music import midi_io
from google.protobuf.json_format import MessageToJson
import json
from datetime import datetime
import assetdb
from ml_midi import*
import numpy as np

#  performance_rnn_generate --config='performance_with_dynamics'
# --bundle_file='../models/jz_performance_rnn.mag' --output_dir='generated/jz_perf'
# --num_outputs=10 --num_steps=3000 --primer_melody="[72,72,75,77]"
#
#

def createTrack(model_name, model_path, num_steps, primer_melody, json_dir, midi_dir):
    if not os.path.exists(model_path):
        print("Error: model file not found: {}.".format(model_path))
        return
    if not os.path.exists(json_dir):
        try:
            os.makedirs(json_dir)
        except OSError:
            print("failed to make dir {}.".format(json_dir))
            return
    if not os.path.exists(midi_dir):
        try:
            os.makedirs(midi_dir)
        except OSError:
            print("failed to make dir {}.".format(midi_dir))
            return
    tmp_dir = midi_dir + 'tmp/'
    if not os.path.exists(tmp_dir):
        try:
            os.makedirs(tmp_dir)
        except OSError:
            print("failed to make dir {}.".format(tmp_dir))
            return
    os.system("performance_rnn_generate --config='performance_with_dynamics' --bundle_file={} \
    --output_dir={} --num_outputs=1 --num_steps={} --primer_melody='[72,72,75,77]'".format(model_path,
    tmp_dir, num_steps))

    # get tmp midi file
    midifiles = glob.glob(tmp_dir + "*.mid")
    # output midifile
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    midi_filepath = midi_dir + "jazz_perf{}.mid".format(timestamp)
    shutil.move(midifiles[0], midi_filepath)

    seq = NoteSequence()
    seq.load_from_file(midi_filepath)
    jsonDict = seq.to_dict()
    print(jsonDict)
    json_filepath = json_dir + "jazz_perf{}.json".format(timestamp)
    # attach asset id
    asset_id = assetdb.AssetDatabase().make_asset_id(json_filepath, timestamp)
    jsonDict['asset_id'] = asset_id

    def default(o):
        if isinstance(o, np.int64): return int(o)
        raise TypeError
    jsonString = json.dumps(jsonDict, default=default)
    with open(json_filepath, "w") as f:
        f.write(jsonString)

    return json_filepath, midi_filepath, timestamp
