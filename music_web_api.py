#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, glob
#from bottledaemon import daemon_run
from bottle import route, get, run, template, static_file, request, response
import json
import web_api_conf as conf
import nn_models
import performance_rnn, improv_rnn, drums_rnn, jazz_performance_rnn, neural_melody
import assetdb

@route(conf.kRouteMusicAPI)
def index():
    return static_file('index.html', root='./')

#----------------------------------------------#

def generate_performance_asset(duration, temperature):
    if duration is not None:
        duration = float(duration)
    else:
        duration = 30.0
    if temperature is not None:
        temperature = float(temperature)
    else:
        temperature = 1.0
    model_name = conf.kPerformanceRNNModelName
    model_path = conf.kPerformanceRNNModelPath
    dest_dir = conf.kPerformanceRNNWorkDir
    json_dir_tmpl = dest_dir + "json/d{}_t{}/"
    midi_dir_tmpl = dest_dir + "mid/d{}_t{}/"
    json_dir = json_dir_tmpl.format(duration, temperature)
    midi_dir = midi_dir_tmpl.format(duration, temperature)
    print("performance_rnn: createTrack duration:{}[sec] temperature:{}".format(duration, temperature))
    json_filepath, midi_filepath, timestamp = performance_rnn.createTrack(model_name, model_path, None, duration, temperature, json_dir, midi_dir)
    print("generated: {} {}".format(json_filepath, midi_filepath))

    db = assetdb.AssetDatabase()
    params = {conf.ParamKeys.Duration:duration, conf.ParamKeys.Temperature:temperature}
    asset_id = db.make_asset_id(json_filepath, timestamp)
    db.write(asset_id, json_filepath, model_name, params=params)
    #asset_id = db.make_asset_id(midi_filepath, timestamp)
    #db.write(asset_id, midi_filepath, model_name, params=params)
    return json_filepath, midi_filepath


def get_performance_assetlist(duration, temperature):
    if duration is not None:
        duration = float(duration)
    if temperature is not None:
        temperature = float(temperature)
    model_name = conf.kPerformanceRNNModelName
    model_path = conf.kPerformanceRNNModelPath
    db = assetdb.AssetDatabase()
    query = {'model_name':model_name, conf.ParamKeys.Duration:duration, conf.ParamKeys.Temperature:temperature}
    result_dict = db.search(query)
    return result_dict

def get_asset(asset_id):
    print("requested get asset:{}".format(asset_id))

    db = assetdb.AssetDatabase()
    result_dict = db.read(asset_id)
    if result_dict is None:
        print("Error: asset {} is not found in DB.".format(asset_id))
        return {"{}"}
    asset_path = result_dict.get('filepath')
    if asset_path is None:
        print("Error: asset filepath is not recorded. {}".format(asset_path))
        return("{}")
    if not os.path.exists(asset_path):
        print("Error: asset is not found.")
        return("{}")
    # get asset data

    f = open(asset_path)
    jsonString = f.read()
    f.close()
    return jsonString


@route(conf.kRoutePerformanceGenerateAsset, method='GET')
def performance():
    duration = request.query.get(conf.ParamKeys.Duration)
    temperature = request.query.get(conf.ParamKeys.Temperature)
    print("requested duration:{} [sec], temperature:{} sequence".format(duration, temperature))
    json_filepath, midi_filepath = generate_performance_asset(duration, temperature)
    return static_file(os.path.basename(json_filepath), root=os.path.dirname(json_filepath))


@route(conf.kRoutePerformanceGenerateAsset, method='POST')
def performance():
    print("header Content-Type: {contentType}")
    if request.get_header('Content-Type') == "application/json":
        print(request.json)

    duration = request.forms.get(conf.ParamKeys.Duration)
    temperature = request.forms.get(conf.ParamKeys.Temperature)
    print("requested duration:{} [sec], temperature:{} sequence".format(duration, temperature))
    json_filepath, midi_filepath = generate_performance_asset(duration, temperature)
    return static_file(os.path.basename(json_filepath), root=os.path.dirname(json_filepath))

@route(conf.kRoutePerformanceGetAssetList, method='GET')
def performance():
    print("requested asset list")
    duration = request.query.get(conf.ParamKeys.Duration)
    temperature = request.query.get(conf.ParamKeys.Temperature)
    jsonDict = get_performance_assetlist(duration, temperature)
    jsonString = json.dumps(jsonDict)
    response.content_type = 'application/json'
    return jsonString

@route(conf.kRoutePerformanceGetAssetList, method='POST')
def performance():
    print("requested asset list")
    duration = request.forms.get(conf.ParamKeys.Duration)
    temperature = request.forms.get(conf.ParamKeys.Temperature)
    asset_list = get_performance_assetlist(duration, temperature)
    jsonDict = {"asset_list":asset_list}
    jsonString = json.dumps(jsonDict)
    response.content_type = 'application/json'
    return jsonString

@route(conf.kRoutePerformanceGetAsset, method='GET')
def on_get_asset():
    asset_id = request.query.get(conf.ParamKeys.Id)
    jsonString = get_asset(asset_id)
    response.content_type = 'application/json'
    return jsonString

#---------------------------------------------------------------------------------------#
# jazz performance

def generate_jazz_performance_asset(num_steps):
    if num_steps is not None:
        num_steps = int(num_steps)
    else:
        num_steps = 1000

    model_name = conf.kJazzPerformanceRNNModelName
    model_path = conf.kJazzPerformanceRNNModelPath
    dest_dir = conf.kJazzPerformanceRNNWorkDir
    json_dir_tmpl = dest_dir + "json/n{}/"
    midi_dir_tmpl = dest_dir + "mid/n{}/"
    json_dir = json_dir_tmpl.format(num_steps)
    midi_dir = midi_dir_tmpl.format(num_steps)
    print("jazz performance rnn: createTrack num_steps:{}[step]".format(num_steps))
    # todo
    primer_melody = [72,72,75,77]
    json_filepath, midi_filepath, timestamp = jazz_performance_rnn.createTrack(model_name, model_path, num_steps, primer_melody, json_dir, midi_dir)
    print("generated: {} {}".format(json_filepath, midi_filepath))

    # register DB
    db = assetdb.AssetDatabase()
    params = {conf.ParamKeys.Numsteps:num_steps}
    asset_id = db.make_asset_id(json_filepath, timestamp)
    db.write(asset_id, json_filepath, model_name, params=params)
    #asset_id = db.make_asset_id(midi_filepath, timestamp)
    #db.write(asset_id, midi_filepath, model_name, params=params)
    return json_filepath, midi_filepath


def get_jazz_performance_assetlist(num_steps):
    if num_steps is not None:
        num_steps = int(num_steps)
    model_name = conf.kJazzPerformanceRNNModelName
    model_path = conf.kJazzPerformanceRNNModelPath
    db = assetdb.AssetDatabase()
    query = {'model_name':model_name, conf.ParamKeys.Numsteps:num_steps}
    result_dict = db.search(query)
    return result_dict

@route(conf.kRouteJazzPerformanceGenerateAsset, method='GET')
def performance():
    numsteps = request.query.get(conf.ParamKeys.Numsteps)
    print("requested numsteps:{} [steps]".format(numsteps))
    json_filepath, midi_filepath = generate_jazz_performance_asset(numsteps)
    return static_file(os.path.basename(json_filepath), root=os.path.dirname(json_filepath))


@route(conf.kRouteJazzPerformanceGenerateAsset, method='POST')
def performance():
    print("header Content-Type: {contentType}")
    if request.get_header('Content-Type') == "application/json":
        print(request.json)

    numsteps = request.forms.get(conf.ParamKeys.Numsteps)
    print("requested numsteps:{} [steps]".format(numsteps))
    json_filepath, midi_filepath = generate_jazz_performance_asset(numsteps)
    return static_file(os.path.basename(json_filepath), root=os.path.dirname(json_filepath))

@route(conf.kRouteJazzPerformanceGetAssetList, method='GET')
def performance():
    print("requested asset list")
    numsteps = request.query.get(conf.ParamKeys.Numsteps)
    jsonDict = get_jazz_performance_assetlist(numsteps)
    jsonString = json.dumps(jsonDict)
    response.content_type = 'application/json'
    return jsonString


@route(conf.kRouteJazzPerformanceGetAsset, method='GET')
def on_get_asset():
    asset_id = request.query.get(conf.ParamKeys.Id)
    jsonString = get_asset(asset_id)
    response.content_type = 'application/json'
    return jsonString


#-------------------------------------------------------#

def generate_improv_asset(backing_chords, primer_melody):
    if backing_chords is None:
        backing_chords = "C G Am F C G Am F"
    if primer_melody is None:
        primer_melody = "60, -2, 60, -2, 67, -2, 67, -2"
    model_name = conf.kImprovRNNModelName
    model_path = conf.kImprovRNNModelPath
    dest_dir = conf.kImprovRNNWorkDir
    json_dir_tmpl = dest_dir + "json/"
    midi_dir_tmpl = dest_dir + "mid/"
    json_dir = json_dir_tmpl#.format(duration, temperature)
    midi_dir = midi_dir_tmpl#.format(duration, temperature)
    print("improv_rnn: create improvisation")

    # backing_chords = "C G Am F C G Am F"
    # primer_melody = "60, -2, 60, -2, 67, -2, 67, -2"
    json_filepath, midi_filepath, timestamp = improv_rnn.createAsset(model_name, model_path, primer_melody, backing_chords, json_dir, midi_dir)
    print("generated: {} {}".format(json_filepath, midi_filepath))

    db = assetdb.AssetDatabase()
    params = {'primer_melody':primer_melody, 'backing_chords':backing_chords}
    asset_id = db.make_asset_id(json_filepath, timestamp)
    db.write(asset_id, json_filepath, model_name, params=params)
    asset_id = db.make_asset_id(midi_filepath, timestamp)
    db.write(asset_id, midi_filepath, model_name, params=params)
    return json_filepath, midi_filepath

@route(conf.kRouteImprovGenerateAsset, method='GET')
def improv():
    primer_melody = request.query.get('ml')
    backing_chords = request.query.get('chord')
    print("get query: primer_melody:{}, backing_chords:{}".format(primer_melody, backing_chords))
    json_filepath, midi_filepath = generate_improv_asset(backing_chords, primer_melody)
    response.content_type = 'application/json'
    return static_file(os.path.basename(json_filepath), root=os.path.dirname(json_filepath))

@route(conf.kRouteImprovGenerateAsset, method='POST')
def improv():
    primer_melody = request.forms.get('ml')
    backing_chords = request.forms.get('chord')
    json_filepath, midi_filepath = generate_improv_asset(backing_chords, primer_melody)
    response.content_type = 'application/json'
    return static_file(os.path.basename(json_filepath), root=os.path.dirname(json_filepath))

def get_imporv_assetlist(backing_chords=None):
    model_name = conf.kImprovRNNModelName
    model_path = conf.kImprovRNNModelPath
    db = assetdb.AssetDatabase()
    query = {'model_name':model_name, conf.ParamKeys.BackingChords:backing_chords}
    #if backing_chords is not None:
    #    query = {'model_name':model_name, conf.ParamKeys.BackingChords:backing_chords}
    #else:
    #    query = {'model_name':model_name}
    result_dict = db.search(query)
    return result_dict

@route(conf.kRouteImprovGetAssetList, method='GET')
def performance():
    print("requested asset list")
    backing_chords = request.query.get('chord')
    print("get query: backing_chords:{}".format(backing_chords))
    asset_list = get_imporv_assetlist(backing_chords)
    jsonDict = {"asset_list":asset_list}
    jsonString = json.dumps(jsonDict)
    response.content_type = 'application/json'
    return jsonString

#----------------------------------------------#
#
def generate_drum_asset(numsteps):
    if numsteps is None:
        numsteps = 128
    model_name = conf.kDrumRNNModelName
    model_path = conf.kDrumRNNModelPath
    dest_dir = conf.kDrumRNNWorkDir
    json_dir_tmpl = dest_dir + "json/{}/".format(numsteps)
    midi_dir_tmpl = dest_dir + "mid/{}/".format(numsteps)
    json_dir = json_dir_tmpl#.format(duration, temperature)
    midi_dir = midi_dir_tmpl#.format(duration, temperature)
    print("drum_rnn: create drum")
    json_filepath, midi_filepath, timestamp = drums_rnn.createAsset(model_name, model_path, numsteps, json_dir, midi_dir)
    print("generated: {} {}".format(json_filepath, midi_filepath))
    if json_filepath is None:
        return json_filepath, midi_filepath
    db = assetdb.AssetDatabase()
    params = {conf.ParamKeys.Numsteps:numsteps}
    asset_id = db.make_asset_id(json_filepath, timestamp)
    db.write(asset_id, json_filepath, model_name, params=params)
    #asset_id = db.make_asset_id(midi_filepath, timestamp)
    #db.write(asset_id, midi_filepath, model_name, params=params)
    return json_filepath, midi_filepath

@route(conf.kRouteDrumGenerateAsset, method='GET')
def generate_drum():
    numsteps = request.query.get(conf.ParamKeys.Numsteps)
    json_filepath, midi_filepath = generate_drum_asset(numsteps)
    response.content_type = 'application/json'
    if json_filepath is None:
        return "{}"
    return static_file(os.path.basename(json_filepath), root=os.path.dirname(json_filepath))


@route(conf.kRouteDrumGenerateAsset, method='POST')
def generate_drum():
    numsteps = request.forms.get(conf.ParamKeys.Numsteps)
    json_filepath, midi_filepath = generate_drum_asset(numsteps)
    response.content_type = 'application/json'
    if json_filepath is None:
        return "{}"
    return static_file(os.path.basename(json_filepath), root=os.path.dirname(json_filepath))

def get_drum_assetlist(numsteps):
    if numsteps is None:
        numsteps = 128
    model_name = conf.kDrumRNNModelName
    model_path = conf.kDrumRNNModelPath

    db = assetdb.AssetDatabase()
    query = {'model_name':model_name, conf.ParamKeys.Numsteps:numsteps}
    result_dict = db.search(query)

    jsonString = json.dumps(result_dict)
    return jsonString

@route(conf.kRouteDrumGetAssetList, method='GET')
def get_drumlist():
    print("requested asset list")
    numsteps = request.query.get(conf.ParamKeys.Numsteps)
    jsonString = get_drum_assetlist(numsteps)
    response.content_type = 'application/json'
    return jsonString


@route(conf.kRouteDrumGetAsset, method='GET')
def on_get_asset():
    asset_id = request.query.get(conf.ParamKeys.Id)
    jsonString = get_asset(asset_id)
    response.content_type = 'application/json'
    return jsonString

#contentType = request.get_header('Content-Type')
#print("header Content-Type: {contentType}")
#if contentType == "application/json":
#print(request.json)

#----------------------------------------------------------------#
# neural melody

def generate_neural_melody_chord_progression_asset():
    model_name = conf.kNeuralMelodyModelName
    model_path = conf.kNeuralMelodyModelPath
    dest_dir = conf.kNeuralMelodyWorkDir
    json_dir = dest_dir + model_name + "/json/"
    print("neural melody chords: createTrack")
    json_filepath, timestamp = neural_melody.createChordProgression(model_name, model_path, json_dir)
    print("generated: {}".format(json_filepath))

    if json_filepath is None:
        return json_filepath

    db = assetdb.AssetDatabase()
    params = {}
    asset_id = db.make_asset_id(json_filepath, timestamp)
    db.write(asset_id, json_filepath, model_name, params=params)
    #asset_id = db.make_asset_id(midi_filepath, timestamp)
    #db.write(asset_id, midi_filepath, model_name, params=params)
    return json_filepath


def get_neural_melody_assetlist():
    model_name = conf.kNeuralMelodyModelName
    model_path = conf.kNeuralMelodyModelPath
    db = assetdb.AssetDatabase()
    query = {'model_name':model_name}
    result_dict = db.search(query)
    return result_dict

@route(conf.kRouteNeuralMelodyChordGenerateAsset, method='GET')
def generate_drum():
    json_filepath  = generate_neural_melody_chord_progression_asset()
    response.content_type = 'application/json'
    if json_filepath is None:
        return "{}"
    return static_file(os.path.basename(json_filepath), root=os.path.dirname(json_filepath))

@route(conf.kRouteNeuralMelodyGetyChordAssetList, method='GET')
def get_nmelody_chord_list():
    print("requested asset list")
    jsonString = get_neural_melody_assetlist()
    response.content_type = 'application/json'
    return jsonString


#----------------------------------------------------------------#
# get asset


@route(conf.kRouteGetAsset, method='GET')
def on_get_asset():
    asset_id = request.query.get(conf.ParamKeys.Id)
    jsonString = get_asset(asset_id)
    response.content_type = 'application/json'
    return jsonString





#----------------------------------------------#

# Static Routes
@get('/<filename:re:.*\.js>')
def javascripts(filename):
    return static_file(filename, root='static/js')

@get('/<filename:re:.*\.json>')
def jon(filename):
    return static_file(filename, root='static/json')

@get('/<filename:re:.*\.css>')
def stylesheets(filename):
    return static_file(filename, root='static/css')

@get('/<filename:re:.*\.mid>')
def stylesheets(filename):
	return static_file(filename, root='static/mid')

@get('/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
    return static_file(filename, root='static/img')


@get('/<filename:re:.*\.(eot|ttf|woff|svg)>')
def fonts(filename):
    return static_file(filename, root='static/fonts')

# download model files
model_dir = conf.kModelDir
nn_models.download_models(model_dir)

run(host='localhost', port=8880)
# externally accessible server
#run(host='0.0.0.0', port=8880, debug=True)
#daemon_run(host='0.0.0.0', port=8880)
