#!/usr/bin/env python
# -*- coding: utf-8 -*-

import parse_neural_melody_output as nm_parser
import json, os
from datetime import datetime
import assetdb
from ml_midi import*

kEnableNeuralMelody = True

# https://github.com/mil-tokyo/NeuralMelody
# part: verse pre-chorus chorus and bridge
# chords in key C

def createChordProgression(model_name, model_path, json_dir):
    if not kEnableNeuralMelody:
        print("kEnableNeuralMelody is set False: import NeuralMelody module and set True")
        return None, None

    if not os.path.exists(model_path):
        print("Error: model file not found: {}.".format(model_path))
        return None, None
    if not os.path.exists(json_dir):
        try:
            os.makedirs(json_dir)
        except OSError:
            print("failed to make dir {}.".format(json_dir))
            return None, None
    # os.system("python chord_sequence_generation.py --num_gen 1 --DEBUG True")
    os.system("python generate_midi.py --gen_chords True checkpoint/model_checkpoint_music_7c648eb1fe03_baseline_1129.14_40.00.p")

    tmp_dir = "results_chords/"
    # get tmp midi file
    gen_file = tmp_dir + "0.txt"

    # parse
    chord_progressions = nm_parser.parse_chord_progression_output(gen_file)

    # create output json dict
    jsonDict = {'chord_progressions': chord_progressions, 'model': 'neural_melody'}
    jsonDict['tempo'] = 120.0
    print(jsonDict)

    ## load melody midi file
    melody_midi_filepath = "generate_test.mid"
    seq = NoteSequence()
    seq.load_from_file(melody_midi_filepath)
    melody_dict = seq.to_dict()
    print(melody_dict)
    jsonDict['melody'] = melody_dict

    # output json file
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    json_filepath = json_dir + "nm_chord_prog{}.json".format(timestamp)
    # attach asset id
    asset_id = assetdb.AssetDatabase().make_asset_id(json_filepath, timestamp)
    print(asset_id)
    jsonDict['asset_id'] = asset_id

    def default(o):
        if isinstance(o, np.int64): return int(o)
        raise TypeError
    jsonString = json.dumps(jsonDict, default=default)
    with open(json_filepath, "w") as f:
        f.write(jsonString)

    return json_filepath, timestamp


if __name__ == "__main__":
    kNeuralMelodyModelName = "neural_melody"
    kNeuralMelodyModelPath = 'checkpoint/model_checkpoint_music_7c648eb1fe03_baseline_1129.14_40.00.p'
    model_name = kNeuralMelodyModelName
    model_path = kNeuralMelodyModelPath
    kNeuralMelodyWorkDir = './'
    dest_dir = kNeuralMelodyWorkDir
    json_dir = dest_dir + model_name + "/json/"

    json_filepath, timestamp = createTrack('neural_melody_chords', model_path, json_dir)
    print(json_filepath, timestamp)
