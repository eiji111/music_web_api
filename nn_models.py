#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import google_drive_downloader

def load_model_paths(model_path_json):
    if not os.path.exists(model_path_json):
        print("Error: model path config file is not found.")
        print("key: model file name, value: google drive file id")
        return None
    with open(model_path_json) as f:
        jsonString = f.read()
    jsonDict = json.loads(jsonString)
    return jsonDict

import google_drive_downloader

def download_nn_models_from_google_drive(google_drive_file_id, model_filename, dest_dir):
    # https://drive.google.com/open?id=xxxxxxxxxxxxx
    # model_filename: performance_rnn.mag
    # dest_dir: mag/
    model_filepath = dest_dir + model_filename
    if os.path.exists(model_filepath):
        print("Model file {} exists.".format(model_filepath))
        return
    print("Model file {} not found.".format(model_filepath))
    print("Start downloading from google drive...")
    #google_drive_file_id = 'xxxxxxxxxxxxx'
    destination = model_filepath
    google_drive_downloader.download_file_from_google_drive(google_drive_file_id, destination)
    print("Finished downloading.")

def download_models(model_dir):
    if not os.path.exists(model_dir):
        print("Model dir doesn't exist {}".format(model_dir))
        try:
            os.makedirs(model_dir)
            print("Make dir: {}".format(model_dir))
        except OSError:
            print("failed to make dir {}.".format(model_dir))
            return
    model_paths = load_model_paths(kModelPathJsonPath)
    for model_filename, google_drive_file_id in model_paths.items():
        print("Required model: {}. id: {}".format(model_filename, google_drive_file_id))
        download_nn_models_from_google_drive(google_drive_file_id, model_filename, model_dir)

kModelPathJsonPath = 'model_path.json'
'''
model_path.json example: "key: model file name, value: google drive file id"
{
    "performance_with_dynamics.mag":"123456",
    "chord_pitches_improv.mag":"7890123",
    "drum_kit_rnn":"0987654",
    "attention_rnn":"342126"
}
'''
