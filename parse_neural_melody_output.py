#!/usr/bin/env python
# -*- coding: utf-8 -*-

kPartTable = {0:'verse', 1:'prechorus', 2:'chorus', 3:'bridge'}

def parse_chord_progression_output(gen_filepath, verbose=False):
    with open(gen_filepath, 'r') as f:
        content = f.read()
    lines = content.split('\n')
    chord_progressions = []
    for line in lines:
        if line == '':
            continue
        one_chord_prog = {}
        part, ch0, ch1, ch2, ch3 = line.split(',')
        part = int(part)
        #print(part, ch0, ch1, ch2, ch3)
        if kPartTable.get(part) is None:
            print("Warning: invalid part is obtained. part = {}".format(part))
            continue
        one_chord_prog['part'] = kPartTable[part]
        one_chord_prog['chord_progression'] = [ch0, ch1, ch2, ch3]
        chord_progressions.append(one_chord_prog)
    if verbose: print(chord_progressions)
    return chord_progressions

if __name__ == "__main__":
    chord_progressions = parse_chord_progression_output('results_chords/0.txt')
    print(chord_progressions)
