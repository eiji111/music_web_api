#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from magenta.models.performance_rnn import performance_sequence_generator
from magenta.protobuf import generator_pb2
from magenta.protobuf import music_pb2

import magenta.music as mm

# output midi
from magenta.music import midi_io

# json
from google.protobuf.json_format import MessageToJson
import json
from datetime import datetime
import assetdb
from ml_midi import*
import numpy as np

#BUNDLE_DIR = 'mag/'
#MODEL_NAME = 'performance_with_dynamics'
#BUNDLE_NAME = MODEL_NAME + '.mag'
#model_path = os.path.join(BUNDLE_DIR, BUNDLE_NAME)

def createTrack(model_name, model_path, sequence, endtime, temperature, json_dir, midi_dir):
    if not os.path.exists(model_path):
        print("Error: model file not found: {}.".format(model_path))
        return
    if not os.path.exists(json_dir):
        try:
            os.makedirs(json_dir)
        except OSError:
            print("failed to make dir {}.".format(json_dir))
            return
    if not os.path.exists(midi_dir):
        try:
            os.makedirs(midi_dir)
        except OSError:
            print("failed to make dir {}.".format(midi_dir))
            return

    MODEL_NAME = os.path.basename(model_path)
    # generate track
    bundle = mm.sequence_generator_bundle.read_bundle_file(model_path)
    generator_map = performance_sequence_generator.get_generator_map()
    generator = generator_map[model_name](checkpoint=None, bundle=bundle)
    generator.initialize()
    generator_options = generator_pb2.GeneratorOptions()
    generator_options.args['temperature'].float_value = temperature # Higher is more random; 1.0 is default.
    generate_section = generator_options.generate_sections.add(start_time=0, end_time=endtime)
    sequence = generator.generate(music_pb2.NoteSequence(), generator_options)

    # output midifile
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    midi_filepath = midi_dir + "performance{}.mid".format(timestamp)
    midi_io.sequence_proto_to_midi_file(sequence, midi_filepath,
    drop_events_n_seconds_after_last_note=None)

    # output track as json file
    jsonString = MessageToJson(sequence)
    jsonDict = json.loads(jsonString)
    json_filepath = json_dir + "performance{}.json".format(timestamp)
    # attach asset id
    asset_id = assetdb.AssetDatabase().make_asset_id(json_filepath, timestamp)
    jsonDict['asset_id'] = asset_id

    jsonString = json.dumps(jsonDict)
    #with open(json_filepath, "w") as f:
    #    f.write(jsonString)


    seq = NoteSequence()
    seq.load_from_file(midi_filepath)
    jsonDict = seq.to_dict()
    json_filepath = json_dir + "improv{}.json".format(timestamp)
    # attach asset id
    asset_id = assetdb.AssetDatabase().make_asset_id(json_filepath, timestamp)
    jsonDict['asset_id'] = asset_id

    def default(o):
        if isinstance(o, np.int64): return int(o)
        raise TypeError
    jsonString = json.dumps(jsonDict, default=default)
    with open(json_filepath, "w") as f:
        f.write(jsonString)

    return json_filepath, midi_filepath, timestamp
