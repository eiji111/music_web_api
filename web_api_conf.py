#!/usr/bin/env python
# -*- coding: utf-8 -*-

# models
kModelDir = 'mag/'

# music_api

# routing
kRouteMusicAPI = '/music_api/'
kRouteGetAsset = kRouteMusicAPI + 'get'

# params
class ParamKeys:
    Id = "id"
    Duration = "duration"
    Temperature = "temperature"
    Numsteps = "step"
    BackingChords = "backing_chords"
# performance RNN
# models
kPerformanceRNNWorkDir = 'performance/'
kPerformanceRNNModelName = "performance_with_dynamics"
kPerformanceRNNModelPath = kModelDir + 'performance_with_dynamics.mag'

# routing
kRoutePerformance = kRouteMusicAPI + 'perf/'
kRoutePerformanceGenerateAsset = kRoutePerformance + 'gen'
kRoutePerformanceGetAssetList = kRoutePerformance + 'list'
kRoutePerformanceGetAsset = kRoutePerformance + 'get'

# http://localhost:8880/music_api/performance/gen?duration=30

#---------------------------------------------------------------#
# jazz performance RNN
# models
kJazzPerformanceRNNWorkDir = 'jazz_performance/'
kJazzPerformanceRNNModelName = "jazz_performance_with_dynamics"
kJazzPerformanceRNNModelPath = kModelDir + 'jazz_performance_with_dynamics.mag'

# routing
kRouteJazzPerformance = kRouteMusicAPI + 'jazz_perf/'
kRouteJazzPerformanceGenerateAsset = kRouteJazzPerformance + 'gen'
kRouteJazzPerformanceGetAssetList = kRouteJazzPerformance + 'list'
kRouteJazzPerformanceGetAsset = kRouteJazzPerformance + 'get'

#---------------------------------------------------------------#

# imporv RNN
# models
kImprovRNNWorkDir = 'improv/'
kImprovRNNModelName = "chord_pitches_improv"
kImprovRNNModelPath = kModelDir + 'chord_pitches_improv.mag'

# routing
kRouteImprov = kRouteMusicAPI + 'improv/'
kRouteImprovGenerateAsset = kRouteImprov + 'gen'
kRouteImprovGetAssetList = kRouteImprov + 'list'
kRouteImprovGetAsset = kRouteImprov + 'get'


# drum RNN
kDrumRNNWorkDir = 'drum/'
kDrumRNNModelName = "drum_kit_rnn"
kDrumRNNModelPath = kModelDir + 'drum_kit_rnn.mag'

# routing
kRouteDrum = kRouteMusicAPI + 'drum/'
kRouteDrumGenerateAsset = kRouteDrum + 'gen'
kRouteDrumGetAssetList = kRouteDrum + 'list'
kRouteDrumGetAsset = kRouteDrum + 'get'


#---------------------------------------------#
# neural melody
kNeuralMelodyWorkDir = './'
kNeuralMelodyModelName = "neural_melody"
kNeuralMelodyModelPath = 'checkpoint/model_checkpoint_music_7c648eb1fe03_baseline_1129.14_40.00.p'

# routing
kRouteNeuralMelody = kRouteMusicAPI + 'nmelody/'
kRouteNeuralMelodyChordGenerateAsset = kRouteNeuralMelody + 'gen'
kRouteNeuralMelodyGetyChordAssetList = kRouteNeuralMelody + 'list'

# http://localhost:8880/music_api/nmelody/gen

# database
kAssetDatabase = 'assetdb.json'
kLockDir = 'music_api_lock'
